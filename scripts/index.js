import authorization from "./components/authorization";
import setBoardSize from "./components/board";
import changeSlide from "./components/slider";
import { generateCards, shuffleCards, unflipCards } from "./components/cards";
import setTimeLimit from "./components/timer";


const gridContainer = document.querySelector(".grid-container");
const modal = document.querySelector('.modal');
const auth = document.querySelector('.auth');
const gameWindow = document.querySelector('.game'),
      menuWindow = document.querySelector('.menu');
const scoreTableItemsEasy = document.querySelectorAll('[data-level="easy"]'),
      scoreTableItemsMiddle = document.querySelectorAll('[data-level="middle"]'),
      scoreTableItemsHard = document.querySelectorAll('[data-level="hard"]');

let cards = [];
let arrayEasy = [],
    arrayMiddle = [],
    arrayHard = [];
let setTime;
let firstCard, secondCard;
let lockBoard = false;
let score = 0;


function defineLevelTable(storageKey, recordsArray, selector) {

    if (window.localStorage.getItem(storageKey)) {
        getFromStorage(storageKey, selector);
        recordsArray = [...JSON.parse(window.localStorage.getItem(storageKey))];
    }
}

defineLevelTable('scoreEasy', arrayEasy, scoreTableItemsEasy);
defineLevelTable('scoreMiddle', arrayMiddle, scoreTableItemsMiddle);
defineLevelTable('scoreHard', arrayHard, scoreTableItemsHard);

document.querySelector(".score").textContent = score;

document.querySelectorAll('.modal__btn').forEach(btn => {
    btn.addEventListener('click', () => {
        hideModal(btn);
    });
});

document.querySelector('.aside__btn').addEventListener('click', (e) => {
    hideModal(e.target);
})

function getData(n) {
    fetch("./database/cards.json")
        .then((res) => res.json())
        .then((database) => {

            for (let i = 0; i <= n; i++) {
                cards.push(...database);
            }

            shuffleCards(cards);
            generateCards(cards, gridContainer, flipCard);
            setTime = setTimeLimit(gridContainer, showModal, toggleModalContent, setTime);
        });
}

function flipCard() {
    if (lockBoard) return;
    if (this === firstCard) return;

    this.classList.add("flipped");

    if (!firstCard) {
        firstCard = this;
        return;
    }

    secondCard = this;
    score++;
    document.querySelector(".score").textContent = score;
    lockBoard = true;

    checkForMatch();
    openModal();
}

function checkForMatch() {
    let isMatch = firstCard.dataset.name === secondCard.dataset.name;
    isMatch ? disableCards() : unflipCards(firstCard, secondCard, resetBoard);
}

function disableCards() {
    firstCard.removeEventListener("click", flipCard);
    secondCard.removeEventListener("click", flipCard);

    resetBoard();
}


function resetBoard() {
    firstCard = null;
    secondCard = null;
    lockBoard = false;
}

const restartBtns = document.querySelectorAll('.restart');
restartBtns.forEach(btn => btn.addEventListener('click', restart));

function restart() {
    resetBoard();
    shuffleCards(cards);
    score = 0;
    document.querySelector(".score").textContent = score;
    gridContainer.innerHTML = "";
    generateCards(cards, gridContainer, flipCard);
}

function openModal() {
    const renderedCards = document.querySelectorAll('.card');
    let counter = 0;

    for (let i of renderedCards) {
        if (i.classList.contains('flipped')) {
            counter++;
        }
    }

    if (counter == cards.length) {
        updateScore();
        clearInterval(setTime);
        toggleModalContent(0);
        setTimeout(showModal, 1000);
    }
}

function showModal() {
    modal.style.display = 'flex';
}

function hideModal(btn) {
    
    clearInterval(setTime);
    
    if (btn.classList.contains('restart')) {
        setTime = setTimeLimit(gridContainer, showModal, toggleModalContent, setTime);
    }

    if (btn.classList.contains('to-menu')) {
        menuWindow.style.display = 'block';
        gameWindow.style.display = 'none';
        cards = [];
        restart();
    }
    modal.style.display = 'none';
}

function updateScore() {
    const score = document.getElementById('score');
    const scoreModal = document.querySelector('.modal__score');

    scoreModal.innerText = score.innerText;

    if (gridContainer.classList.contains('hard')) {
        defineStorage('scoreHard', arrayHard, scoreTableItemsHard);
    } else if (gridContainer.classList.contains('middle')) {
        defineStorage('scoreMiddle', arrayMiddle, scoreTableItemsMiddle);
    } else {
        defineStorage('scoreEasy', arrayEasy, scoreTableItemsEasy);
    }

    function defineStorage(storageKey, recordsArray, selector) {

        if (window.localStorage.getItem(storageKey)) {
            recordsArray = [...JSON.parse(window.localStorage.getItem(storageKey))];
        }

        recordsArray.push(score.innerText);

        if (selector.length < recordsArray.length) {
            sortNumbers(recordsArray).pop();
        }

        window.localStorage.setItem(storageKey, JSON.stringify(sortNumbers(recordsArray)));

        getFromStorage(storageKey, selector);
    }
}

function getFromStorage(key, selector) {
    let records = JSON.parse(window.localStorage.getItem(key));

    for (let i = 0; i < records.length; i++) {
        records.forEach((item, n) => {
            selector[n].innerText = item;
        });
    }
}

function sortNumbers(array) {
    return array.sort(function(a, b) {
        return a - b;
    });
}

function toggleModalContent(num) {
    const modalContent = document.querySelectorAll('.modal__content');

    modalContent.forEach((content, i) => {
        if (i == num) {
            content.classList.add('modal__content_active');
        } else {
            content.classList.remove('modal__content_active');
        }
    });
}

authorization(auth, menuWindow);
setBoardSize(menuWindow, gameWindow, getData);
changeSlide();