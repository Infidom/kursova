/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./scripts/components/authorization.js":
/*!*********************************************!*\
  !*** ./scripts/components/authorization.js ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* binding */ authorization)
/* harmony export */ });
function authorization(auth, menuWindow) {
  const form = document.querySelector('.auth__form');
  const userLogin = document.getElementById('login');
  const userPassword = document.getElementById('pass');
  function isAuth() {
    const user = window.localStorage.getItem('login');
    if (user) {
      const loginWindow = document.createElement('div');
      loginWindow.classList.add('auth__body');
      loginWindow.innerHTML = `
                <h2 class="auth__title">Hello, ${user}</h2>
                <p class="auth__text">Type your password to continue</p>
                <form class="auth__form_log">
                    <div class="auth__fields">
                        <input type="password" class="auth__input" name="password" placeholder="Enter password" id="pass-log" required>
                    </div>
                    <button class="modal__btn auth__btn">Log in</button>
                </form>
            `;
      auth.innerHTML = '';
      auth.insertAdjacentElement('beforeend', loginWindow);
    }
  }
  function allowAccess() {
    const password = document.getElementById('pass-log');
    if (password.value == window.localStorage.getItem('password')) {
      auth.style.display = 'none';
      menuWindow.style.display = 'block';
    } else {
      document.querySelector('.auth__form_log').reset();
    }
  }
  auth.addEventListener('submit', e => {
    e.preventDefault();
    const target = e.target;
    if (target && target.classList.contains('auth__form_log')) {
      allowAccess();
    }
  });
  form.addEventListener('submit', e => {
    e.preventDefault();
    window.localStorage.setItem('login', userLogin.value);
    window.localStorage.setItem('password', userPassword.value);
    auth.style.display = 'none';
    menuWindow.style.display = 'block';
  });
  isAuth();
}

/***/ }),

/***/ "./scripts/components/board.js":
/*!*************************************!*\
  !*** ./scripts/components/board.js ***!
  \*************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* binding */ setBoardSize)
/* harmony export */ });
function setBoardSize(menuWindow, gameWindow, getData) {
  const grid = document.querySelector('.grid-container');
  const tabs = document.querySelectorAll('.menu__tab');
  tabs.forEach(tab => {
    tab.addEventListener('click', () => {
      if (tab == tabs[0]) {
        grid.classList.remove('grid-container_large', 'middle', 'hard');
        getData(1);
        menuWindow.style.display = 'none';
        gameWindow.style.display = 'flex';
      } else if (tab == tabs[1]) {
        grid.classList.add('grid-container_large', 'middle');
        grid.classList.remove('hard');
        getData(3);
        menuWindow.style.display = 'none';
        gameWindow.style.display = 'flex';
      } else {
        grid.classList.add('grid-container_large', 'hard');
        grid.classList.remove('middle');
        getData(5);
        menuWindow.style.display = 'none';
        gameWindow.style.display = 'flex';
      }
    });
  });
}

/***/ }),

/***/ "./scripts/components/cards.js":
/*!*************************************!*\
  !*** ./scripts/components/cards.js ***!
  \*************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   generateCards: () => (/* binding */ generateCards),
/* harmony export */   shuffleCards: () => (/* binding */ shuffleCards),
/* harmony export */   unflipCards: () => (/* binding */ unflipCards)
/* harmony export */ });
function generateCards(cards, gridContainer, flipCard) {
  for (let card of cards) {
    const cardElement = document.createElement("div");
    cardElement.classList.add("card");
    cardElement.setAttribute("data-name", card.name);
    cardElement.innerHTML = `
            <div class="front">
                <img class="front-image" src=${card.image} />
            </div>
            <div class="back"></div>
        `;
    gridContainer.appendChild(cardElement);
    cardElement.addEventListener("click", flipCard);
  }
}
function shuffleCards(cards) {
  let currentIndex = cards.length,
    randomIndex,
    temporaryValue;
  while (currentIndex !== 0) {
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;
    temporaryValue = cards[currentIndex];
    cards[currentIndex] = cards[randomIndex];
    cards[randomIndex] = temporaryValue;
  }
}
function unflipCards(firstCard, secondCard, resetBoard) {
  setTimeout(() => {
    firstCard.classList.remove("flipped");
    secondCard.classList.remove("flipped");
    resetBoard();
  }, 1000);
}


/***/ }),

/***/ "./scripts/components/slider.js":
/*!**************************************!*\
  !*** ./scripts/components/slider.js ***!
  \**************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* binding */ changeSlide)
/* harmony export */ });
function changeSlide() {
  const tables = document.querySelectorAll('.records__group');
  const arrowLeft = document.querySelector('[data-arrow="left"]');
  const arrowRight = document.querySelector('[data-arrow="right"]');
  let count = 0;
  arrowLeft.addEventListener('click', () => {
    count--;
    setSlide();
  });
  arrowRight.addEventListener('click', () => {
    count++;
    setSlide();
  });
  function setSlide() {
    tables.forEach(table => table.style.display = 'none');
    if (count < 0) {
      count = tables.length - 1;
    }
    if (count > tables.length - 1) {
      count = 0;
    }
    tables[count].style.display = 'block';
  }
}

/***/ }),

/***/ "./scripts/components/timer.js":
/*!*************************************!*\
  !*** ./scripts/components/timer.js ***!
  \*************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
const mins = document.querySelector('.minutes'),
  secs = document.querySelector('.seconds');
function setTimeLimit(gridContainer, showModal, toggleModalContent, setTime) {
  if (gridContainer.classList.contains('hard')) {
    return updateTime(160, showModal, toggleModalContent, setTime);
  } else if (gridContainer.classList.contains('middle')) {
    return updateTime(110, showModal, toggleModalContent, setTime);
  } else {
    return updateTime(60, showModal, toggleModalContent, setTime);
  }
}
function updateTime(time, showModal, toggleModalContent, setTime) {
  let total = time;
  let minutes, seconds;
  calcTime(total);
  setTime = setInterval(() => {
    total -= 1;
    calcTime(total);
  }, 1000);
  function calcTime(total) {
    minutes = Math.floor(total / 60);
    seconds = Math.floor(total % 60);
    mins.innerText = getZero(minutes);
    secs.innerText = getZero(seconds);
    if (total <= 0) {
      showModal();
      clearInterval(setTime);
      toggleModalContent(1);
    }
  }
  return setTime;
}
function getZero(num) {
  if (num >= 0 && num < 10) {
    return `0${num}`;
  } else {
    return num;
  }
}
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (setTimeLimit);

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be isolated against other modules in the chunk.
(() => {
/*!**************************!*\
  !*** ./scripts/index.js ***!
  \**************************/
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _components_authorization__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./components/authorization */ "./scripts/components/authorization.js");
/* harmony import */ var _components_board__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./components/board */ "./scripts/components/board.js");
/* harmony import */ var _components_slider__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./components/slider */ "./scripts/components/slider.js");
/* harmony import */ var _components_cards__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./components/cards */ "./scripts/components/cards.js");
/* harmony import */ var _components_timer__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./components/timer */ "./scripts/components/timer.js");





const gridContainer = document.querySelector(".grid-container");
const modal = document.querySelector('.modal');
const auth = document.querySelector('.auth');
const gameWindow = document.querySelector('.game'),
  menuWindow = document.querySelector('.menu');
const scoreTableItemsEasy = document.querySelectorAll('[data-level="easy"]'),
  scoreTableItemsMiddle = document.querySelectorAll('[data-level="middle"]'),
  scoreTableItemsHard = document.querySelectorAll('[data-level="hard"]');
let cards = [];
let arrayEasy = [],
  arrayMiddle = [],
  arrayHard = [];
let setTime;
let firstCard, secondCard;
let lockBoard = false;
let score = 0;
function defineLevelTable(storageKey, recordsArray, selector) {
  if (window.localStorage.getItem(storageKey)) {
    getFromStorage(storageKey, selector);
    recordsArray = [...JSON.parse(window.localStorage.getItem(storageKey))];
  }
}
defineLevelTable('scoreEasy', arrayEasy, scoreTableItemsEasy);
defineLevelTable('scoreMiddle', arrayMiddle, scoreTableItemsMiddle);
defineLevelTable('scoreHard', arrayHard, scoreTableItemsHard);
document.querySelector(".score").textContent = score;
document.querySelectorAll('.modal__btn').forEach(btn => {
  btn.addEventListener('click', () => {
    hideModal(btn);
  });
});
document.querySelector('.aside__btn').addEventListener('click', e => {
  hideModal(e.target);
});
function getData(n) {
  fetch("./database/cards.json").then(res => res.json()).then(database => {
    for (let i = 0; i <= n; i++) {
      cards.push(...database);
    }
    (0,_components_cards__WEBPACK_IMPORTED_MODULE_3__.shuffleCards)(cards);
    (0,_components_cards__WEBPACK_IMPORTED_MODULE_3__.generateCards)(cards, gridContainer, flipCard);
    setTime = (0,_components_timer__WEBPACK_IMPORTED_MODULE_4__["default"])(gridContainer, showModal, toggleModalContent, setTime);
  });
}
function flipCard() {
  if (lockBoard) return;
  if (this === firstCard) return;
  this.classList.add("flipped");
  if (!firstCard) {
    firstCard = this;
    return;
  }
  secondCard = this;
  score++;
  document.querySelector(".score").textContent = score;
  lockBoard = true;
  checkForMatch();
  openModal();
}
function checkForMatch() {
  let isMatch = firstCard.dataset.name === secondCard.dataset.name;
  isMatch ? disableCards() : (0,_components_cards__WEBPACK_IMPORTED_MODULE_3__.unflipCards)(firstCard, secondCard, resetBoard);
}
function disableCards() {
  firstCard.removeEventListener("click", flipCard);
  secondCard.removeEventListener("click", flipCard);
  resetBoard();
}
function resetBoard() {
  firstCard = null;
  secondCard = null;
  lockBoard = false;
}
const restartBtns = document.querySelectorAll('.restart');
restartBtns.forEach(btn => btn.addEventListener('click', restart));
function restart() {
  resetBoard();
  (0,_components_cards__WEBPACK_IMPORTED_MODULE_3__.shuffleCards)(cards);
  score = 0;
  document.querySelector(".score").textContent = score;
  gridContainer.innerHTML = "";
  (0,_components_cards__WEBPACK_IMPORTED_MODULE_3__.generateCards)(cards, gridContainer, flipCard);
}
function openModal() {
  const renderedCards = document.querySelectorAll('.card');
  let counter = 0;
  for (let i of renderedCards) {
    if (i.classList.contains('flipped')) {
      counter++;
    }
  }
  if (counter == cards.length) {
    updateScore();
    clearInterval(setTime);
    toggleModalContent(0);
    setTimeout(showModal, 1000);
  }
}
function showModal() {
  modal.style.display = 'flex';
}
function hideModal(btn) {
  clearInterval(setTime);
  if (btn.classList.contains('restart')) {
    setTime = (0,_components_timer__WEBPACK_IMPORTED_MODULE_4__["default"])(gridContainer, showModal, toggleModalContent, setTime);
  }
  if (btn.classList.contains('to-menu')) {
    menuWindow.style.display = 'block';
    gameWindow.style.display = 'none';
    cards = [];
    restart();
  }
  modal.style.display = 'none';
}
function updateScore() {
  const score = document.getElementById('score');
  const scoreModal = document.querySelector('.modal__score');
  scoreModal.innerText = score.innerText;
  if (gridContainer.classList.contains('hard')) {
    defineStorage('scoreHard', arrayHard, scoreTableItemsHard);
  } else if (gridContainer.classList.contains('middle')) {
    defineStorage('scoreMiddle', arrayMiddle, scoreTableItemsMiddle);
  } else {
    defineStorage('scoreEasy', arrayEasy, scoreTableItemsEasy);
  }
  function defineStorage(storageKey, recordsArray, selector) {
    if (window.localStorage.getItem(storageKey)) {
      recordsArray = [...JSON.parse(window.localStorage.getItem(storageKey))];
    }
    recordsArray.push(score.innerText);
    if (selector.length < recordsArray.length) {
      sortNumbers(recordsArray).pop();
    }
    window.localStorage.setItem(storageKey, JSON.stringify(sortNumbers(recordsArray)));
    getFromStorage(storageKey, selector);
  }
}
function getFromStorage(key, selector) {
  let records = JSON.parse(window.localStorage.getItem(key));
  for (let i = 0; i < records.length; i++) {
    records.forEach((item, n) => {
      selector[n].innerText = item;
    });
  }
}
function sortNumbers(array) {
  return array.sort(function (a, b) {
    return a - b;
  });
}
function toggleModalContent(num) {
  const modalContent = document.querySelectorAll('.modal__content');
  modalContent.forEach((content, i) => {
    if (i == num) {
      content.classList.add('modal__content_active');
    } else {
      content.classList.remove('modal__content_active');
    }
  });
}
(0,_components_authorization__WEBPACK_IMPORTED_MODULE_0__["default"])(auth, menuWindow);
(0,_components_board__WEBPACK_IMPORTED_MODULE_1__["default"])(menuWindow, gameWindow, getData);
(0,_components_slider__WEBPACK_IMPORTED_MODULE_2__["default"])();
})();

/******/ })()
;
//# sourceMappingURL=bundle.js.map