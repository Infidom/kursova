export default function setBoardSize( menuWindow, gameWindow, getData) {
    const grid = document.querySelector('.grid-container');
    const tabs = document.querySelectorAll('.menu__tab');

    tabs.forEach(tab => {
        tab.addEventListener('click', () => {
            if (tab == tabs[0]) {
                grid.classList.remove('grid-container_large', 'middle', 'hard');
                getData(1);
                menuWindow.style.display = 'none';
                gameWindow.style.display = 'flex';
            } else if (tab == tabs[1]) {
                grid.classList.add('grid-container_large', 'middle');
                grid.classList.remove('hard');
                getData(3);
                menuWindow.style.display = 'none';
                gameWindow.style.display = 'flex';
            } else {
                grid.classList.add('grid-container_large', 'hard');
                grid.classList.remove('middle');
                getData(5);
                menuWindow.style.display = 'none';
                gameWindow.style.display = 'flex';
            }
        });
    });
}