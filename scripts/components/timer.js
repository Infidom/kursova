const mins = document.querySelector('.minutes'),
      secs = document.querySelector('.seconds');

function setTimeLimit(gridContainer, showModal, toggleModalContent, setTime) {

    if (gridContainer.classList.contains('hard')) {
        return updateTime(160, showModal, toggleModalContent, setTime);
    } else if (gridContainer.classList.contains('middle')) {
        return updateTime(110, showModal, toggleModalContent, setTime);
    } else {
        return updateTime(60, showModal, toggleModalContent, setTime);
    }
}

function updateTime(time, showModal, toggleModalContent, setTime) {
    let total = time;
    let minutes, seconds;

    calcTime(total);

    setTime = setInterval(() => {
        total -= 1;
        calcTime(total);
    }, 1000);

    function calcTime(total) {
        minutes = Math.floor(total / 60);
        seconds = Math.floor(total % 60);
        mins.innerText = getZero(minutes);
        secs.innerText = getZero(seconds);

        if (total <= 0) {
            showModal();
            clearInterval(setTime);
            toggleModalContent(1);
        }
    }

    return setTime;
}

function getZero(num) {
    if (num >= 0 && num < 10) {
        return `0${num}`;
    } else {
        return num;
    }
}

export default setTimeLimit;